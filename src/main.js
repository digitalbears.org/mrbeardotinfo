import initState from "../.initState.json";
import TShirtShop from "./TShirtShop.svelte";

const app = new TShirtShop({
    target: document.body,
    props: {
        tshirts: initState
    }
});

export default app;
const express = require("express");
const app = express();
const port = 3000;

app.use(express.json());

app.post("/order", orderHandler);

function orderHandler(req, res) {
    // TODO validateOrder(req.body);
}

app.listen(port, () => console.log(`Listening on ${port}`));

const crypto = require("crypto");
const hash = crypto.createHash("sha256", "something");
console.log(hash.digest("hex"));